# Cloud Security - An Introduction

This [repository](https://gitlab.com/eduards-talks/cloud-security-an-introduction) contains
a talk about the fundamentals of cloud security for public cloud settings.

## What this talk is

A talk for people with little to no knowledge about cloud security. It exposes them to
some key terms and concepts to get them started on their journey into this world.
It is by no means complete and certainly biased by what the author finds most important.

While it is not vendor specific, the author mostly works with AWS so the terminology,
might have a tinge.

## What this talk is not

This talk is not an in-depth detailed description of how to secure your cloud
environment. It does not talk about threat modeling or compliance requirements in
any meaningful way. It does not show you how to build or maintain asset registers
of your resources of data. It does not cover the whole depth and breadth of hybrid or
multi-cloud setups. It is also distinctly *not* a Kubernetes talk.

## Outline

As the talk is not yet written this is a rough outline of what the talk will likely contain.
Be aware that content and order is subject to change.
Most likely things will get dropped/added based on time and narrative flow.

The talk will in its final form have a duration of around 40-45 minutes.
The times in brackets are planning aids and not final.

- Speaker Introduction (2 min.)
- What is this Cloud thing anyways (4 min.)
- Shared Responsibility Model (10 min.)
- Least Privilege and Strong Identity (10 min.)
- Traceability (aka. Auditability) (5 min.)
- Data Protection (3 min.)
- Multi Account Strategy (2 min.)
- QnA (5-10 min.)
- Further reading and Resources (Just some slides. No talking.)

### If there is much time left

which there won't be:

- Incident Response
- Blast Radius and allies
