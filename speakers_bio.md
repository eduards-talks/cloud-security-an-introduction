# Speakers Bio

Eduard started his journey into security back in university and moved through
multiple roles in his career. From building networks for small and medium businesses,
through penetration testing and test automation framework development to a defensive
security role.
Currently, he is on a team that provides operations and security expertise for
Europe's leading gas capacity trading platform. As DevSecOps Engineer he is responsible
for the security of cloud-based workloads in a regulated environment.
