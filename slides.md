<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

## An Introduction to Cloud Security
<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->

Note:

Hello everyone and welcome to my introduction to cloud security talk.

----  ----

## About

- Eduard Thamm (@crashdco)
- DevSecOps since before it was a thing
- PenTester, Teacher, Automator, Defender (not in order)
- Mostly just curious about the world
- Based in Dresden/Germany

Note:
2 Minutes - Done

----

## Just Some Remarks

- We here to clear a path for others to make their experiences better and more secure.
- Thank you for being interested in Security and investing your time.
- Diversity and different perspectives greatly increase our capability to defend.
- Security can be a hard and frustrating field. It can also be very rewarding.
- Welcome to the world of reading all the manuals, spotting the difference and finding what they did not talk about.

Note:
This usually comes at the end but:

A caveat I give with all of my talks. We have very little time to talk about very complex topics.
Some things I simplified. Some things I left out. I tried to do so diligently.

----  ----

## Content

- What is Cloud?
- Shared Responsibility Model
- Identity and Access Management
- Audit Trails
- Data Protection
- Multi Account Strategy
- Your Allies

----  ----

## What is this Cloud Thing Anyways

1. A way to outsource 'undifferentiated' heavy lifting
1. A radically different financial model
1. Someone elses computer
1. Several layers of additional abstraction to use for convenience (& profit) (IaaS,PaaS,SaaS,FaaS)

Note:
4 Minutes - Done

----

## A Short Story

_Let's say you just created the next big thing._

An amazing Django App that will blow the world away. Now all you need to do is
run it on something that is not your laptop.

Note:
Back when I started in IT you would now go around and shop for servers, and rack space, and
after that you would learn some Linux administration skills, oh and database administration,
and, and, and...

True you could always hire people to do that but who has that kind of money?
This would take months, maybe years if you wanted to do all of those things.

Until then, your idea would be dated and out of touch. You would have missed your
window of opportunity.

Today you could spin up a cloud account, use some form of service mix to get your stuff
on the Internet plain and simple. You pay for what you forget to turn off and that's it.

----  ----

## Shared Responsibility Model

_So that will be yours, but all of this is mine._

Note:
10 Minutes - Done

This is one of the, if not the most important concept in cloud security.
It delineates what you are responsible for and what your cloud provider
will secure for you.

----

## Pizza As A Service

_by Albert Barron_

![Pizza As A Service Image](images/pizza-as-a-service.jpeg) <!-- .element height="75%" width="75%" -->

Note:

You are always to some extend responsible for the things you own and use.
Even in the SaaS world. It usually boils down to you are responsible for eating the food
and not choking on it. In IT terms managing access control.

Sadly the reality of IT is not as simple as the Pizza Model.

----

## Cloud Shared Responsibility Model

![Cloud Shared Responsibility](images/shared-responsibility.png)

Note:

The most interesting part to me are the shared sectors. Those would, time permitting
be interesting to explore in more detail and I encourage you to do so. As a general rule
of thumb, if you can in any way configure it, you are responsible for it being secure.

What is middleware in this context? That is hard to say at this level. Let us define it
the things that you need to run that sit between your application and the operating system.

EKS: Management vs Workload

----  ----

## Least Privilege and Strong Identity

- Who do you claim to be?
  - How can you prove that to me?
- Ok, and now what are you allowed to do?
  - Which actions?
  - Which resources?
  - Any special conditions?

Note:
10 Minutes - Done

Identity and Access management is perhaps the most important set of controls you
can implement.
If you look at the last few years of the Verizon Data Breach Investigation Report,
you will see that compromised credentials are amongst the most used ways to
achieve attack goals.

The main bullet points above encompass two distinct concepts. We will discuss them
separately. They are often confused or misused. It is vital to keep them straight and
well separated in your minds and implementations.

----

## Identity - Authentication

_Who are you?_

- To proof you are who you say you are
- How to proof
  - Something you know (password, api key,...)
  - Something you have (U2F, Phone with TOTP,...)
  - Something you are (fingerprint, face scan,...)
- Do not share identity
- Instance Metadata

Note:

Does not only apply to people but also to processes/hosts/...sometimes literature
refers to those as principals.

MFA

Core principles do not change in the cloud. There just are some extensions.
Things get slightly more tricky when you have to authenticate on-prem workloads
against cloud APIs but there are some reasonable patterns here.

----

## Access Management - Authorization

_Now that I know who you are, what are you allowed to do?_

- Least Privilege
  - Enough to get the job done. Nothing more
  - Deny by default
- Separation of duties
  - If you can do it, you should not be able to alter the logs for it
- Centralized Management (PEP/PDP/PAP)
- Roles vs Groups
- MPA

Note:

On-prem: Think about the person with domain admin that gets mimikatz'ed. All the deletes
will be from their account. In Cloud environments it is quite usual that you have to think about workload authorization.

PEP Policy Enforcement Point - At the point of action

PDP Policy Decision Point - The point where a deny allow decision is taken (local/remote)

PAP Policy Administration Point - Manage all the things

Groups - set of principals. Does not state what they are allowed to do

Roles - set of permissions. Does not state who is allowed to do that

----  ----

## Traceability (aka. Auditability)

You usually want to answer these questions:

- What happened?
  - Which assets where effected?
  - Which actions where performed?
- When did it happen?
- Who did it?
- (Why did they do it?)

Note:
5 Minutes - Done

----

## Gather and Analyze Data

- If you work with IaaS little changes in terms of data you want to collect on that side
- Your cloud provider usually allows collection of API activity
- There are higher level service to get you started quickly on the cloud side
- Be careful about data collection. Check with your legals on what you are allowed to collect

Note:
Think server logs, auditd, netflow. All of these exist either as they are on prem
or in some slightly different form -> VpcFlowLogs

GuardDuty, CloudTrail, Azure Monitor

Keep in mind that GDPR actually supports CSIRT activity. Andrew Cormack has some nice
papers and presentations on that.

----

## Keep it hidden. Keep it safe.

- Ship your security relevant logs to a location that an attacker can not reach without substantial effort
- Make sure tampering with your logs is hard
- Store your logs so that tampering can be recognized

Note:
Let me be clear, security by obscurity is not good advice if it is the only security measure you have.
Another dedicated account with limited access usually works well here.

Tamper evident storage could for example be achieved through storing files and digest or thorough
cloud provider primitives.
Adding some form of write once semantic (Object Lock) and enforcing MFA on delete.

----  ----

## Data Protection

- Understand what you are protecting and put it into classes (eg. Low, Medium, High)
- Draw some data flow pictures to understand where data is coming from and where it is going
- Put labels on your data sinks
- Encrypt all the things

Note:
3 Minutes - Done

Talk to your legals and business. Ask questions like:

- What would happen if, we can not access X for 5 weeks?
- What if Y was posted on a public website?

Data flow do not need to be in depth, box per system is fine. This is just an overview.
Labels are sometimes called Tags, something like 'Data Classification'/'High' is good.
Do use the crypto mechanisms of your cloud provider. Your threat model will likely not prohibit that.

----  ----

## Multi Account Strategy

You should have more than one account in the cloud for any non trivial thing!

- It creates blast zones and clearly defines their radius
- It enables good practices in CI/CD
- It allows for clear separation of concern and cost
- Automation is key to success
- Services help
- The lines you draw should align with your business needs and protection priorities

Note:
2 Minutes - Done

Services: as usual the more sophisticated your needs the less likely for off the shelve to work

----  ----

# Allies

_It's dangerous to go alone! Take this._

Note:
2 Minutes

All of this can not be done alone. Luckily security interests are aligned with other
key aspects of your business. Some will be obvious, others might not be.
All here as in the whole talk is true in my experience. Your mileage may vary.

----

## The Well Known

- Operations
- Development
- Legal & Compliance

Note:

- Ops often charged with upholding the A in CIA
- Development often indirectly charged with upholding the I in CIA (customers will complain about corrupted data as bugs)
- Legal is often seen as one requirement provider to sec. But they can be way more. Talk to them about data classification for example.
- Learn to speak their language, help them learn yours, lend a hand even in non-security related things. Do not complain but
be active and help out. Everyone is busy.

----

## The (Sadly) Less Obvious

- UI/UX
- Product
- Finance

Note:

- UI/UX cares about how users feel. Safe and secure are valued feelings. Theater can have value and be the vehicle for change. It is also a double edged sword.
- Product will usually design a lot of processes and flows. They in my experience care deeply about the user. Catching all the requirements can be very tricky. Talk to them, participate in reviews. Do not lecture and look for ways to help simplifying things.
- Finance usually has or develops an interest in what happens in your cloud and tracing costs to centers. Talk to them about tagging strategies and auditability of environments. Tracing costs helps them and you and their audit experience should not be underestimated.


----

## The Ones I did Not Talk about

- Everybody else

Note:

Everyone in your business is a potential ally. Approach them with an open ear. Listen to what the need and want. Tie that into
the security agenda. Security is a people business.

----  ----

## Q&A

----  ----

## Further Reading and Resources

- Your Cloud Vendors Guide on Security
  - AWS Security Pillar
  - GCP Security Foundations Guide
  - Azure Architecture Framework Security
- Practical Cloud Security: A Guide for Secure Design and Deployment (Chris Dotson)
- [Pizza As A Service](https://www.linkedin.com/pulse/20140730172610-9679881-pizza-as-a-service/)
- [GDPR and CSIRT Starting Point](https://www.first.org/blog/20171211_GDPR_for_CSIRTs)
- [Multi Account Starting Point](https://aws.amazon.com/blogs/industries/defining-an-aws-multi-account-strategy-for-telecommunications-companies/)
